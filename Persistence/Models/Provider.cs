﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models
{
    public class Provider : BaseModel
    {
        public string Name { get; set; }

        public string  Description { get; set; }

        public int OriginId { get; set; }
    }
}
