﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models
{
    public class IdentityOrder : BaseModel
    {
        public string IdentityUserName { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public int Age { get; set; }

        public int Status { get; set; }

        public double TotalPrice { get; set; }

    }
}
