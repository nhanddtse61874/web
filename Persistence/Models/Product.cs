﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models
{
    public class Product : BaseModel
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public string  Description { get; set; }

        public double Price { get; set; }

        public double CurrentPrice { get; set; }

        public double SalePercent { get; set; }

        public int Quantity { get; set; }

        public bool Status { get; set; }

        public bool DisplayStatus { get; set; }

        public string ImagePath { get; set; }

        public string ProductTypeId { get; set; }

        public int OriginId { get; set; }

        public int CategoryId { get; set; }

        public int ProviderId { get; set; }

        
    }
}
