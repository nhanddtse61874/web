﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models
{
    public class Comment : BaseModel
    {
        public string Detail { get; set; }

        public string UserName { get; set; }

        public DateTime StartDate { get; set; }

        public int UserId { get; set; }
    }
}
