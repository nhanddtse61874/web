﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models
{
    public class Order : BaseModel
    {
        public int UserId { get; set; }

        public double TotalPrice { get; set; }

        public DateTime Date { get; set; }

        public int Status { get; set; }
    }
}
