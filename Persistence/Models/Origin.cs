﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models
{
    public class Origin : BaseModel
    {
        public string Name { get; set; }
    }
}
