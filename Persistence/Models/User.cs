﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models
{
    public class User : BaseModel
    {
        public string  UserName{ get; set; }

        public string Password { get; set; }

        public string Name { get; set; }

        public string  Address { get; set; }

        public string Phone { get; set; }

        public bool Sex { get; set; }

        public int Age { get; set; }

        public bool Status { get; set; }
    }
}
