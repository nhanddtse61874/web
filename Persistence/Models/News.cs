﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models
{
    public class News : BaseModel
    {

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public bool Status { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public int UserId { get; set; }
    }
}
